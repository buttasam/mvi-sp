# MVI - Semestr project
Generating piano music using LSTM neural network with simple evolution mechanism.

### Basic usage
Run jupiter notebook (see semestral_mvi.ipynb).

Import files and define constants.
```python
import music_rnn
import music_evolution
import music_manipulation
import time

epochs=200
notes_history_length=10
lstm_cell_count=64
```

Train model. Returned object contains trained model and some other data.
```python
train_result = music_rnn.train("data/in/mozart_small/", epochs=epochs, notes_history_length=notes_history_length, lstm_cell_count=lstm_cell_count)
```

Now you can generate music. This command will generate output files in root directory. 
```python
init_inputs = music_rnn.generateAll(train_result, ouput_files_count=10, notes_history_length=notes_history_length)
```

You can evolve initial vector. Second parameter contains boolean flags that you can evolve ouput file.
```python
evolved_input = music_evolution.evolveInput(init_inputs, [False, True, True, False, False, True, True, False, False, False])
# you can generate and save single file
# generate evolved  
generatedTones = music_rnn.generate(train_result["model"], evolved_input, train_result["dataset"], output_size=50)
music_manipulation.storeMidi(music_manipulation.noteToMidi(generatedTones, offset_delta=0.5), "evolved-output-" + str(time.time()) + ".midi")
```

### Dependencies
Use python 3.
```
pip3 install tensorflow
pip3 install py-midi
```