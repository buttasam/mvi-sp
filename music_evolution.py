import random

def randomInput(notes_count, ouput_size):
    return random.sample(range(0, notes_count), ouput_size) 


def evolveInput(init_inputs, inputs_bools):
    # filter only selected
    selected = []
    for i in range(len(init_inputs)):
        if inputs_bools[i]:
            selected.append(init_inputs[i])
    # assertion
    if(len(selected) < 2):
        print("assertion error, must select min 2 values")
        return
    evolved = []
    selected_random = random.randint(0, len(selected) - 1)
    for i in range(len(inputs_bools)):
        evolved.append(selected[selected_random][i])
    return evolved
