import numpy
import os
from keras.utils import np_utils
from music21 import converter, instrument, note, chord, stream

# helper funciton for music manipulation

def allFilesToNotes(path):
    result = []
    for filename in os.listdir(path):
        print("Processing file: " + path + filename)
        result = numpy.append(result, oneFileToNotes(path + filename))
    return result

def oneFileToNotes(midi_file_path):
    notes = []
    midi = converter.parse(midi_file_path)
    notes_to_parse = None
    parts = instrument.partitionByInstrument(midi)
    if parts: # file has instrument parts
        notes_to_parse = parts.parts[0].recurse()
    else: # file has notes in a flat structure
        notes_to_parse = midi.flat.notes
    for element in notes_to_parse:
        if isinstance(element, note.Note):
            notes.append(str(element.pitch))
        elif isinstance(element, chord.Chord):
            notes.append('.'.join(str(n.pitch) for n in element))
    return notes


def noteToMidi(output_notes, offset_delta=0.5):
    offset = 0
    output = []
    for output_note in output_notes:
        # pattern is a chord
        if ('.' in output_note):
            notes_in_chord = output_note.split('.')
            notes = []
            for current_note in notes_in_chord:
                new_note = note.Note(current_note)
                new_note.storedInstrument = instrument.Piano()
                notes.append(new_note)
            new_chord = chord.Chord(notes)
            new_chord.offset = offset
            output.append(new_chord)
        # pattern is a note
        else:
            new_note = note.Note(output_note)
            new_note.offset = offset
            new_note.storedInstrument = instrument.Piano()
            output.append(new_note)
        # increase offset each iteration so that notes do not stack
        offset += offset_delta
    return output


def storeMidi(output_midi, file_name):
    midi_stream = stream.Stream(output_midi)
    midi_stream.write('midi', fp=file_name)


def mapNotesToNumbers(notes):
    unique_notes = sorted(set(item for item in notes))
    return dict((note, number) for number, note in enumerate(unique_notes))


def prepareDataset(notes, notes_history_length):
    notes_count = len(set(notes))
    sequence_length = 5
    note_to_int = mapNotesToNumbers(notes)
    input = []
    output = []

    # create input sequences and the corresponding outputs
    for i in range(0, len(notes) -  notes_history_length, 1):
        sequence_in = notes[i:i +  notes_history_length]
        sequence_out = notes[i +  notes_history_length]
        input.append([note_to_int[char] for char in sequence_in])
        output.append(note_to_int[sequence_out])
    n_patterns = len(input)
    # reshape the input into a format compatible with LSTM layers
    input = numpy.reshape(input, (n_patterns,  notes_history_length, 1))
    # normalize input
    input = input / float(notes_count)
    output = np_utils.to_categorical(output)

    return {"input": input, "output": output, "notes_count": notes_count, "note_to_int": note_to_int}
