import tensorflow as tf
from tensorflow.keras import layers
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers import Activation
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint
import music_manipulation
import music_evolution
from music21 import converter, instrument, note, chord, stream
import music_rnn
import numpy
import random
import time


#define model
def createModel(notes_count, lstm_cell_count): 
    print("Creating model with notes count: ", notes_count, "and LSTM cells: ", lstm_cell_count)
    model = Sequential()
    model.add(LSTM(lstm_cell_count))
    #model.add(Dropout(0.3))
    model.add(Dense(notes_count, activation="softmax"))
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    return model

# train model
def trainModel(model, input, output, epochs, store_improvement=False):
    if store_improvement:
        filepath = "weights-improvement-{epoch:02d}-{loss:.4f}-bigger.hdf5"    
        checkpoint = ModelCheckpoint(
            filepath, monitor='loss', 
            verbose=0,        
            save_best_only=True,        
            mode='min'
        )    
        callbacks_list = [checkpoint]
    else:
        callbacks_list = []    
    model.fit(input, output, epochs=epochs, batch_size=64, callbacks=callbacks_list)

# main train function
def train(midi_path, epochs,  notes_history_length, lstm_cell_count=64):
    input_notes = music_manipulation.allFilesToNotes(midi_path)
    dataset = music_manipulation.prepareDataset(input_notes,  notes_history_length)
    model = music_rnn.createModel(dataset["notes_count"], lstm_cell_count)
    music_rnn.trainModel(model, dataset["input"], dataset["output"], epochs)
    return {"model": model, "dataset": dataset}
    
# main generate function
def generate(model, input_notes, dataset, output_size = 500):
    map_int_to_notes = dict([[v,k] for k,v in dataset["note_to_int"].items()])
    prediction_output = []
    # first input
    first_input = input_notes

    for i in range(output_size):
        prediction_input = numpy.reshape(first_input, (1, len(first_input), 1))
        prediction_input = prediction_input / float(dataset["notes_count"])
        prediction = model.predict(prediction_input, verbose=0)
        note_int = numpy.argmax(prediction)
        prediction_output = numpy.append(prediction_output, map_int_to_notes[note_int])

        first_input = numpy.append(first_input, note_int)
        first_input = first_input[1:len(first_input)]
        
    return prediction_output


def generateAll(train_result, ouput_files_count, notes_history_length):
    init_inputs = []
    for i in range(ouput_files_count):
        randomInput = music_evolution.randomInput(train_result["dataset"]["notes_count"], notes_history_length)
        init_inputs.append(randomInput)

        generatedTones = music_rnn.generate(train_result["model"], randomInput, train_result["dataset"], output_size=notes_history_length)
        music_manipulation.storeMidi(music_manipulation.noteToMidi(generatedTones, offset_delta=0.5), "output-" + str(i) + "-" + str(time.time()) + ".midi")
    return init_inputs